<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
		 pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--suppress HtmlUnknownTarget, HtmlUnknownTarget -->
<html>
<head>
	<%-- Favicon START--%>
	<link rel="apple-touch-icon" sizes="57x57" href="${pageContext.request.contextPath}/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="${pageContext.request.contextPath}/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="${pageContext.request.contextPath}/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="${pageContext.request.contextPath}/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="${pageContext.request.contextPath}/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="${pageContext.request.contextPath}/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="${pageContext.request.contextPath}/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/favicon-194x194.png" sizes="194x194">
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/android-chrome-192x192.png"
		  sizes="192x192">
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="${pageContext.request.contextPath}/manifest.json">
	<link rel="mask-icon" href="${pageContext.request.contextPath}/safari-pinned-tab.svg" color="#01267b">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">
	<meta name="theme-color" content="#01267b">
	<%-- Favicon END --%>
	<%-- Google Analytics START --%>
	<script>
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
						(i[r].q = i[r].q || []).push(arguments)
					}, i[r].l = 1 * new Date();
			a = s.createElement(o),
					m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-64202884-1', 'auto');
		ga('send', 'pageview');

	</script>
	<%-- Google Analytics END --%>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link href="${pageContext.request.contextPath}/css/index.css" rel="stylesheet" type="text/css"/>
	<link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<title>Harmelodic</title>
</head>
<body>
<div id="wrapper">
	<div id="content">
		<a id="logoWrapper" href="./">
			<img id="logo" alt="logo" src="${pageContext.request.contextPath}/images/logo.png"/>
		</a>
		<!-- SOFTWARE -->
		<div class="separator"></div>
		<span class="separatorText">Software</span>
		<div class="separator"></div>
		<div class="projectList">
			<a class="project" href="https://github.com/Harmelodic/Android_Layouts" target="_blank">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/Android_Layouts.png">
				<span class="projectTitle">Android Layouts</span>
			</a>
			<a class="project" href="https://github.com/Harmelodic/Example_Bukkit_Plugin" target="_blank">
				<img class="projectImage" alt="P1"
					 src="${pageContext.request.contextPath}/images/Example_Bukkit_Plugin.png">
				<span class="projectTitle">Example Bukkit Plugin</span>
			</a>
			<a class="project" href="https://github.com/Harmelodic/MVC_SimpleFrame" target="_blank">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/MVC_SimpleFrame.png">
				<span class="projectTitle">MVC SimpleFrame</span>
			</a>
			<a class="project" href="https://github.com/Harmelodic/Myrtle" target="_blank">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/Myrtle.png">
				<span class="projectTitle">Myrtle</span>
			</a>
			<a class="project" href="https://github.com/Harmelodic/Palindrome_Checker" target="_blank">
				<img class="projectImage" alt="P1"
					 src="${pageContext.request.contextPath}/images/Palindrome_Checker.png">
				<span class="projectTitle">Palindrome Checker</span>
			</a>
			<a class="project" href="https://github.com/Harmelodic/Quick_Nodejs" target="_blank">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/Quick_Nodejs.png">
				<span class="projectTitle">Quick Nodejs</span>
			</a>
			<a class="project" href="https://github.com/Harmelodic/Read_File" target="_blank">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/Read_File.png">
				<span class="projectTitle">Read File</span>
			</a>
			<a class="project" href="https://github.com/Harmelodic/ResponseStatus_API" target="_blank">
				<img class="projectImage" alt="P1"
					 src="${pageContext.request.contextPath}/images/ResponseStatus_API.png">
				<span class="projectTitle">ResponseStatus API</span>
			</a>
		</div>
		<!-- RESOURCES -->
		<div class="separator"></div>
		<span class="separatorText">Resources</span>
		<div class="separator"></div>
		<div class="projectList">
			<a class="project"
			   href="https://chrome.google.com/webstore/detail/harmelodic-custom/nlgaidjddhmbibdbialabgoligelmknb"
			   target="_blank">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/Chrome_Theme.png">
				<span class="projectTitle">Chrome Theme</span>
			</a>
			<a class="project" href="https://github.com/Harmelodic/harmelodic.www_v1" target="_blank">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/harmelodic.www.png">
				<span class="projectTitle">harmelodic.www v1</span>
			</a>
			<a class="project" href="https://github.com/Harmelodic/harmelodic.www_v2" target="_blank">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/harmelodic.www.png">
				<span class="projectTitle">harmelodic.www v2</span>
			</a>
			<a class="project" href="https://github.com/Harmelodic/harmelodic.www_v3" target="_blank">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/harmelodic.www.png">
				<span class="projectTitle">harmelodic.www v3</span>
			</a>
			<a class="project" href="https://github.com/Harmelodic/MineTrek_Resource_Pack" target="_blank">
				<img class="projectImage" alt="P1"
					 src="${pageContext.request.contextPath}/images/MineTrek_Resource_Pack.png">
				<span class="projectTitle">MineTrek Resource Pack</span>
			</a>
			<a class="project" href="http://nerdcubed-minecraft-resources.s3-eu-west-1.amazonaws.com/" target="_blank">
				<img class="projectImage" alt="P1"
					 src="${pageContext.request.contextPath}/images/Nerd3_Minecraft_Archive.png">
				<span class="projectTitle">Nerd&sup3; Minecraft Archive</span>
			</a>
			<a class="project" href="https://github.com/Harmelodic/Script_Library" target="_blank">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/Script_Library.png">
				<span class="projectTitle">Script Library</span>
			</a>
		</div>
		<!-- MEDIA -->
		<div class="separator"></div>
		<span class="separatorText">Media</span>
		<div class="separator"></div>
		<div class="projectList">
			<a class="project" href="http://blog.harmelodic.com" target="_blank">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/Blog.png">
				<span class="projectTitle">Blog</span>
			</a>
			<a class="project" href="https://www.youtube.com/user/harmelodic" target="_blank">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/YouTube.png">
				<span class="projectTitle">YouTube</span>
			</a>
		</div>
		<!-- MISC. -->
		<div class="separator"></div>
		<span class="separatorText">Misc.</span>
		<div class="separator"></div>
		<div class="projectList">
			<a class="project" href="./about">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/About.png">
				<span class="projectTitle">About</span>
			</a>
			<a class="project" href="./contact">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/Contact.png">
				<span class="projectTitle">Contact</span>
			</a>
			<a class="project" href="https://github.com/Harmelodic" target="_blank">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/GitHub.png">
				<span class="projectTitle">GitHub</span>
			</a>
			<a class="project" href="https://gitlab.com/u/Harmelodic" target="_blank">
				<img class="projectImage" alt="P1" src="${pageContext.request.contextPath}/images/GitLab.png">
				<span class="projectTitle">GitLab</span>
			</a>
		</div>
	</div>
</div>
</body>
</html>
