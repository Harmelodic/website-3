<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
		 pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%-- Favicon START--%>
	<link rel="apple-touch-icon" sizes="57x57" href="${pageContext.request.contextPath}/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="${pageContext.request.contextPath}/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="${pageContext.request.contextPath}/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="${pageContext.request.contextPath}/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="${pageContext.request.contextPath}/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="${pageContext.request.contextPath}/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="${pageContext.request.contextPath}/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/favicon-194x194.png" sizes="194x194">
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/android-chrome-192x192.png"
		  sizes="192x192">
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="${pageContext.request.contextPath}/manifest.json">
	<link rel="mask-icon" href="${pageContext.request.contextPath}/safari-pinned-tab.svg" color="#01267b">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">
	<meta name="theme-color" content="#01267b">
	<%-- Favicon END --%>
	<%-- Google Analytics START --%>
	<script>
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
						(i[r].q = i[r].q || []).push(arguments)
					}, i[r].l = 1 * new Date();
			a = s.createElement(o),
					m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-64202884-1', 'auto');
		ga('send', 'pageview');

	</script>
	<%-- Google Analytics END --%>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link href="${pageContext.request.contextPath}/css/about.css" rel="stylesheet" type="text/css"/>
	<link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.ico">
	<title>About</title>
</head>
<body>
<div id="wrapper">
	<div id="content">
		<a id="logoWrapper" href="./">
			<img id="logo" alt="logo" src="${pageContext.request.contextPath}/images/logo.png"/>
		</a>
		<div class="separator"></div>
		<span class="separatorText">About</span>
		<div class="separator"></div>
		<div id="text">
			<p>
				Harmelodic is the alias of Software Engineer and random dork: Matt Smith.
				<br>
				<br>
				The aim of Harmelodic is to create stuff for people. Whether that be games, software, hardware, ideas,
				designs. Essentially, anything that people can view and/or use that I've created.
				<br>
			</p>
		</div>
	</div>
</div>
</body>
</html>
