<img src="https://github.com/Harmelodic/www_v3/blob/master/Harmelodic.png" alt="Logo" height="100">

# www v3

[![Build Status](https://travis-ci.org/Harmelodic/www_v3.svg?branch=master)](https://travis-ci.org/Harmelodic/www_v3)

The Harmelodic website (v3)

The 3rd version of the harmelodic website. This webapp is for information on Harmelodic and for navigation to other Harmelodic projects.

##### Runs in: Servlet Container (Apache Tomcat)
